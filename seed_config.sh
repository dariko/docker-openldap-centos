#!/bin/bash -x

if [[ -v SEED_CONFIG_JINJA && "${SEED_CONFIG_JINJA}" == "1" ]];then
    jinja2 /seed_config.ldif > /seed_config.ldif.tmp
else
    cp /seed_config.ldif /seed_config.ldif.tmp
fi

for db_path in $(cat /seed_config.ldif.tmp |sed -n 's/^olcdbdirectory: \(.*\)$/\1/pI')
do
    mkdir -p "$db_path"
    chown -R ldap:ldap "$db_path"
done

pid_file=$(sed -n 's/^olcpidfile: \(.*\)$/\1/pI' /seed_config.ldif.tmp)
if [ ! -z "$pid_file" ];then 
    pid_dir="$(dirname $pid_file)"
    rm -f "$pid_file"
    mkdir -p "$pid_dir"
    chown -R ldap:ldap "$pid_dir"
fi

rm -rf "${CONFIG_DIR}"
mkdir "${CONFIG_DIR}"
"$SLAPADD" -n0 -F "${CONFIG_DIR}" -l /seed_config.ldif.tmp
chown -R ldap:ldap "${CONFIG_DIR}"
