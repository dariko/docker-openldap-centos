docker rm -f openldap || true

docker run -d \
    --name openldap \
    --env CN_CONFIG_PASSWORD=cn_config_password \
    --volume $(pwd)/tests/seed_config.ldif:/seed_config.ldif \
    $IMAGE/commit:$CI_COMMIT_SHA
sleep 2

docker exec openldap \
    /usr/local/openldap/bin/ldapsearch \
        -H ldap://localhost \
        -D cn=config \
        -w cn_config_password \
        -b cn=config \
        -s base \
        -LLL cn=config dn | grep ^"dn: cn=config"

docker rm -f openldap
