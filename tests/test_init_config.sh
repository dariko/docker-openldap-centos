docker rm -f openldap || true

docker run -d \
    --name openldap \
    --env  INIT_CONFIG=y \
    $IMAGE/commit:$CI_COMMIT_SHA
sleep 2

docker exec openldap \
    /usr/local/openldap/bin/ldapsearch \
        -H ldap://localhost \
        -D cn=config \
        -w password \
        -b cn=config \
        -s base \
        -LLL cn=config dn | grep ^"dn: cn=config"

docker rm -f openldap
