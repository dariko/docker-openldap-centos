FROM centos:7

COPY ltb-project.repo /etc/yum.repos.d/
COPY RPM-GPG-KEY-LTB-project /etc/pki/rpm-gpg/

ENV OPENLDAP_VERSION=2.4.48
ENV MAKEOPTS=-j4

ARG http_proxy

RUN cp /etc/yum.conf /etc/yum.conf.orig  \
    && [ -n "$http_proxy" ] && echo "proxy = $http_proxy" >> /etc/yum.conf || true \
    && [ -n "$http_proxy" ] && export https_proxy="$http_proxy" || true \
    && yum install -y epel-release \
    && yum install -y python-pip openldap-ltb-$OPENLDAP_VERSION \
                      openldap-ltb-contrib-overlays-$OPENLDAP_VERSION \
    && pip install --no-cache-dir jinja2-cli \
    && yum clean all \
    && rm -rf /var/cache/yum \
    && mv -f /etc/yum.conf.orig /etc/yum.conf

ENV PATH "/usr/local/openldap/bin:/usr/local/openldap/sbin:${PATH}"
ENV SEED_CONFIG_JINJA "1"

EXPOSE 389
EXPOSE 636

COPY run.sh /
COPY init_config.sh /
COPY seed_config.sh /
RUN chmod +x /run.sh /init_config.sh /seed_config.sh
CMD /run.sh
