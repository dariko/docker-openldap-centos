[![Build Status](https://travis-ci.com/dariko/docker-openldap-centos.svg?branch=master)](https://travis-ci.com/dariko/docker-openldap-centos)

### docker-openldap-centos

Docker image for running an openldap service.

Based on ```centos:7```, with packages based on [ltb-project](http://ltb-project.org/).


See [docker-compose.yml](./docker-compose.yml) for examples.
